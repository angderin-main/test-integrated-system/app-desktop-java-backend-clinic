package helper;

/**
 * Created by Derin Anggara on 11/27/2017.
 */
public class User {

    public User(String username, String password, String nama, String tempat, String tanggal, String gender, String alamat, String daerah, String tinggi, String handphone, String email, String noKTP) {
        this.username = username;
        this.password = password;
        this.nama = nama;
        this.tempat = tempat;
        this.tanggal = tanggal;
        this.gender = gender;
        this.alamat = alamat;
        this.daerah = daerah;
        this.tinggi = tinggi;
        this.handphone = handphone;
        this.email = email;
        this.noKTP = noKTP;
    }

    String id, username, password, nama, tempat, tanggal, gender, alamat, daerah, tinggi, handphone, email, noKTP;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getDaerah() {
        return daerah;
    }

    public void setDaerah(String daerah) {
        this.daerah = daerah;
    }

    public String getTinggi() {
        return tinggi;
    }

    public void setTinggi(String tinggi) {
        this.tinggi = tinggi;
    }

    public String getHandphone() {
        return handphone;
    }

    public void setHandphone(String handphone) {
        this.handphone = handphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNoKTP() {
        return noKTP;
    }

    public void setNoKTP(String noKTP) {
        this.noKTP = noKTP;
    }
}
