package sample;

import SocketPackage.SendData;
import helper.Message;
import helper.SettingClass;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import javax.swing.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


/**
 * Created by erwin on 11/8/2017.
 */
public class registerController {

    @FXML
    TextField txtUsername,txtKTP,txtNama,txtEmail;

    @FXML
    PasswordField txtPassword;

    public void onRegisterButtonClicked (){
        try {
            Message message = new Message("signup_user",new String [] {txtUsername.getText(),txtPassword.getText(),txtKTP.getText(),txtNama.getText(),txtEmail.getText()});
            System.out.println(message);
            int[] response = new int[]{0};
            final CountDownLatch latch = new CountDownLatch(1);
            SendData sendData = new SendData(message.toString(), SettingClass.host,SettingClass.portname,response,latch);
            sendData.start();
            latch.await(5, TimeUnit.SECONDS);
            if (response[0]==1){
                System.out.println("Success");
                JFrame frame = new JFrame("Pemberitahuan");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                JOptionPane.showMessageDialog(frame,"User baru berhasil dibuat!");
            }else{
                System.out.println("Failed");
                JFrame frame = new JFrame("Pemberitahuan");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                JOptionPane.showMessageDialog(frame,"gagal membuat user baru!");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


}
