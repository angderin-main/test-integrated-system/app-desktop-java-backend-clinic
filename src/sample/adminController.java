package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by erwin on 11/8/2017.
 */
public class adminController {

    public void handleRegister (ActionEvent event) throws  Exception{

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("register.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle("Register");
        stage.setResizable(false);
        stage.setScene(new Scene(root1, 565, 490));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();

    }


    public void handleUpdate (ActionEvent event) throws  Exception{

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("update.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle("Register");
        stage.setResizable(false);
        stage.setScene(new Scene(root1, 565, 490));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();

    }

    public void handleLogout (ActionEvent event) throws  Exception{

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("login.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle("Register");
        stage.setResizable(false);
        stage.setScene(new Scene(root1, 375, 239));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
        ((Node)(event.getSource())).getScene().getWindow().hide();


    }

}
