package sample;

import SocketPackage.SendData;
import helper.Message;
import helper.SettingClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.swing.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by erwin on 11/8/2017.
 */
public class loginController {
    @FXML
    Button btnLogin;
    @FXML
    TextField usernameTxt;
    @FXML
    PasswordField passwordTxt;

    public void handleLogin(ActionEvent event) throws  Exception{


        final CountDownLatch latch = new CountDownLatch(1);
        int[] response = new int[]{0};

        Message message = new Message("login_admin",new String[]{usernameTxt.getText(),passwordTxt.getText()});
        Thread sendData = new Thread(new SendData(message.toString(), SettingClass.host,SettingClass.portname,response,latch));
        sendData.start();
        latch.await(5, TimeUnit.SECONDS);

        if(response[0]==1) {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("admin.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Admin");
            stage.setResizable(false);
            stage.setScene(new Scene(root1, 385, 228));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
            ((Node)(event.getSource())).getScene().getWindow().hide();
        }
        else {
            JFrame frame = new JFrame("Pemberitahuan");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            JOptionPane.showMessageDialog(frame,"username dan password salah!");
        }

    }





}
