package SocketPackage;

import java.util.concurrent.CountDownLatch;

public class SendData extends Thread {

    private String data = null;
    private String host = null;
    private int portnumber;
    private CountDownLatch latch = null;
    private int[] response = null;

    public SendData(String data, String host, int portnumber,int[] response , CountDownLatch latch) {
        this.data = data;
        this.host = host;
        this.portnumber = portnumber;
        this.latch = latch;
        this.response = response;
    }

    @Override
    public void run() {
        super.run();
        ClientSocketHandler clientSocketHandler = new ClientSocketHandler(host,portnumber);
        clientSocketHandler.OpenSocketClient();
        response[0] = Integer.parseInt(clientSocketHandler.ConnectToServer(data));
        clientSocketHandler.CloseConnection();
        latch.countDown();

        this.stop();
    }
}
